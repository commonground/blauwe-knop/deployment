# Blauwe Knop Deployment

This deployment consists of multiple demo organizations (e.g. Gemeente Tilburg, CJIB, etc).
In addition also a login page and some repositories are required. The manifests can be found in the `dependencies` folder. 
The `dependencies` folder also contains a manifest for a postgres database which is needed for the NLX clusters. The management interface of NLX uses a postgres database to store configuration state.
All of them are required to demonstrate the [Blauwe Knop Mobile App](https://gitlab.com/commonground/blauwe-knop/mobile-app).

The components are packed as [Helm charts](https://helm.sh/) and deployed on the [Haven cluster](https://haven.commonground.nl/) `azure-common-prod` in namespace `bk-test`.

Every organization has multiple components with each their own Helm chart. These can be found in their repositories:

1. [App Debt Process](https://gitlab.com/commonground/blauwe-knop/app-debt-process)
1. [Session Process](https://gitlab.com/commonground/blauwe-knop/session-process)
1. [App Link Process](https://gitlab.com/commonground/blauwe-knop/app-link-process)
1. [Debt Register](https://gitlab.com/commonground/blauwe-knop/debt-register)
1. [Session Register](https://gitlab.com/commonground/blauwe-knop/session-register)
1. [Link Register](https://gitlab.com/commonground/blauwe-knop/link-register)

When the organization is a registrator the following components are also required:

1. [Debt Request Process](https://gitlab.com/commonground/blauwe-knop/debt-request-process)
1. [Debt Request Register](https://gitlab.com/commonground/blauwe-knop/debt-request-register)

Along with aforementioned components also an [NLX cluster](https://docs.nlx.io/understanding-the-basics/introduction) per organization is required.

An NLX cluster consists of the following components:

1. OIDC provider
1. [NLX Management](https://artifacthub.io/packages/helm/commonground/nlx-management)
1. [NLX Outway](https://artifacthub.io/packages/helm/commonground/nlx-outway)
1. For the registrator only: [NLX Inway](https://artifacthub.io/packages/helm/commonground/nlx-inway)

The [GITOPS](https://www.weave.works/technologies/gitops/) tool [Flux](https://docs.fluxcd.io/en/1.19.0/) is used to monitor this repository.
Any changes made to this repository will automatically be deployed on the cluster.

Further more, Flux will monitor the image versions of the components.
When Flux detects a new image version it will push this change to this repository and the deployment on the cluster will be updated.

To learn more about using Flux on a haven cluster please read the [documentation](https://haven.commonground.nl/docs/ci-cd).

## Cluster authentication

- [https://auth.haven.vng.cloud](https://auth.haven.vng.cloud)

## Adding a organization

In order to add a new organization, you need to update the following scripts:

1. `generate_all.sh`,
1. `apply_all.sh`
1. `delete_all.sh`

and the `scheme` repository.

## Deployment

Before running the deployment scripts, make sure you have access to the Kubernetes cluster and namespace.
Also [openssl](https://www.openssl.org), [jq](https://stedolan.githyhub.io/jq/) and [yq](https://mikefarah.github.io/yq/) are required.

First, run the script `generate_all.sh` which will generate all the required manifests.
Prefixing the run script with either `UPDATE_CERTS=true generate_all.sh`, `UPDATE_SCHEMA=true generate_all.sh` or both `UPDATE_CERTS=true UPDATE_SCHEMA=true generate_all.sh`,
will respectively update the NLX certificates, update the schema or update both.

---

> **NOTE:**
>
> Currently NLX does not support to create a certificate with a given OIN in the NLX demo environment. Which causes a new OIN for a organization when `UPDATE_CERTS=true` is set.
>
> Whenever `UPDATE_CERTS=true` is set we need to make sure to update the OIN in the [Scheme Repository](https://gitlab.com/commonground/blauwe-knop/scheme/-/blob/master/organizations.json) and in the `generate_all.sh`.
>
> You can retrieve the OIN from the certificate which is included in the files `organizations/{organizationName}/nlx-outway.yml`. Copy the value of `.spec.values.tls.organization.certificatePEM` to a file.
>
> Now use the command `openssl x509 -subject -noout -in {file}.pem`. The OIN is equal to subject.serialNumber.

---

Second, run the script `apply_all.sh` to apply all the newly generated manifests, create the required secrets and install the organizations.
Prefixing the run script with either `UPDATE_CA_ISSUER=true` will update the CA Issuer certificate, which is needed for the key pair.

Last, check if all the services are running. An easy tool for this is [k9s](https://github.com/derailed/k9s).
Start it with `k9s -n bk-test`, enter `:pods` and enter `ctrl-z` in the view.

By executing `delete_all.sh` all the components will be removed completely from the cluster.

### Single organization update or delete

To update a single organization, run `apply.sh NAME_SLUG UPDATE_CA_ISSUER` e.g. `apply.sh CJIB true`.

To delete a single organization, run `delete.sh NAME_SLUG`.

## Configuration

The inway needs to be configured only at the registrator only, which in our case is `gemeente-tilburg`.

The management UIs can be found at the following URLS

- [management-gemeente-tilburg](https://management-gemeente-tilburg.demo.blauweknop.app) (registrator)
- [management-cjib](https://management-cjib.demo.blauweknop.app)
- `https://management-{organizationName}.demo.blauweknop.app`
- ...

At the registrator we have to configure the default inway first, which can be found in `"Instellingen"`.
Next, we need to add a service, which can be done by selecting `Services` on the left-hand side and selecting the `"+ Service toevoegen"` button, which can be found on the right-hand side.
Make sure the `"Servicenaam"` has value `gemeente-tilburg-debt-request-register` which is equal to the internal Kubernetes service address.
As `"API endpoint URL"` as `http://<internal-domain-name>` e.g. `http://gemeente-tilburg-debt-request-register`.

Finally, scroll down to the bottom and select the corresponding inway and ensure `"Publiceren in de centrale directory"` is checked and press `"Service toevoegen"`, after which it will be visible in the directory.

For all the non-registrator organizations you will need to go in to the mangement environment, find the aforementioned service in the list and request access, after which you will need to approve it in the
registrator management environment by selecting the service.

# Docker compose example for source organization

In the `docker-compose.yml` you can find an example to run as a source organization. You also need the [NLX Outway](https://docs.nlx.io/try-nlx/docker/introduction).
