#!/usr/bin/env bash

export NAME_SLUG="$1"

if [ -z "$NAME_SLUG" ]
then
        echo "Please set \$NAME_SLUG. `apply.sh NAME_SLUG UPDATE_CA_ISSUER`"
fi

export UPDATE_CA_ISSUER="$1"

if [ -z "$UPDATE_CA_ISSUER" ]
then
        echo "Please set \$UPDATE_CA_ISSUER. `apply.sh NAME_SLUG UPDATE_CA_ISSUER`"
fi

export NAMESPACE="bk-test"

if [ -n "${UPDATE_CA_ISSUER}" ]; then
        ./dependencies/create_ca_issuer.sh $NAME_SLUG
fi

echo "################################################################################"
echo "Applying k8s configuration for $NAME_SLUG"
echo "################################################################################"

kubectl apply -f "./organizations/$NAME_SLUG/" \
        --namespace="${NAMESPACE}"

echo "--------------------------------------------------------------------------------"
