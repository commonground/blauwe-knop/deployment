#!/usr/bin/env bash

export UPDATE_CA_ISSUER=${UPDATE_CA_ISSUER:-"false"}

export NAMESPACE="bk-test"

./dependencies/create_wildcard.sh

kubectl -n $NAMESPACE apply -f ./dependencies/redis.yaml
kubectl -n $NAMESPACE apply -f ./dependencies/helm-repos.yaml
kubectl -n $NAMESPACE apply -f ./dependencies/login-page.yaml
kubectl -n $NAMESPACE apply -f ./dependencies/nlx-postgres.yaml

NAME_SLUGS="gemeente-coevorden gemeente-scherpenzeel gemeente-enschede gemeente-tilburg duo gemeente-harlingen gemeente-utrecht svb belastingdienst gemeente-baarn gemeente-hulst gemeente-zeewolde uwv cak gemeente-bergen-nh gemeente-maastricht waterschap-rivierenland cjib gemeente-rotterdam lbio"

for NAME_SLUG in ${NAME_SLUGS}; do
    ./apply.sh $NAME_SLUG $UPDATE_CA_ISSUER
done


echo "!!! ALL DONE !!!"