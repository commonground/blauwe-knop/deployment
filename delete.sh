#!/usr/bin/env bash

export NAME_SLUG="$1"

if [ -z "$NAME_SLUG" ]
then
	echo "Please set \$NAME_SLUG. `init.sh NAME_SLUG`"
fi

export NAMESPACE="bk-test"

echo "################################################################################"
echo "Deleting k8s configuration for $NAME_SLUG"
echo "################################################################################"

kubectl delete -f "./organizations/$NAME_SLUG/" \
        --namespace="${NAMESPACE}"

kubectl delete secret "$NAME_SLUG-dex-ingress-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-internal-ca-key-pair" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-nlx-inway-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-nlx-management-api-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-nlx-outway-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-nlxctl-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-session-tls" \
        --namespace="${NAMESPACE}"
kubectl delete secret "$NAME_SLUG-tls" \
        --namespace="${NAMESPACE}"

echo "--------------------------------------------------------------------------------"