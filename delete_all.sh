#!/usr/bin/env bash
export NAMESPACE="bk-test"

NAME_SLUGS="gemeente-coevorden gemeente-scherpenzeel gemeente-enschede gemeente-tilburg duo gemeente-harlingen gemeente-utrecht svb belastingdienst gemeente-baarn gemeente-hulst gemeente-zeewolde uwv cak gemeente-bergen-nh gemeente-maastricht waterschap-rivierenland cjib gemeente-rotterdam lbio"

for NAME_SLUG in ${NAME_SLUGS}; do
    ./delete.sh $NAME_SLUG
done

kubectl delete -f dependencies/login-page.yaml \
        --namespace="${NAMESPACE}"

kubectl delete -f dependencies/helm-repos.yaml \
        --namespace="${NAMESPACE}"

kubectl delete -f dependencies/redis.yaml \
        --namespace="${NAMESPACE}"
