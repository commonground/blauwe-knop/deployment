#!/usr/bin/env bash

export NAME_SLUG="$1"

if [ -z "$NAME_SLUG" ]
then
	echo "Please set \$NAME_SLUG. `init.sh NAME_SLUG`"
fi


NAMESPACE="bk-test"

echo "################################################################################"
echo "Creating CA K8s secrets for $NAME_SLUG"
echo "################################################################################"

### create secret

CERT="./pki/organizations/${NAME_SLUG}/ca/intermediate.pem"
KEY="./pki/organizations/${NAME_SLUG}/ca/intermediate-key.pem"

### delete existing secret
kubectl delete secret "${NAME_SLUG}-internal-ca-key-pair" \
        --namespace="${NAMESPACE}"

kubectl create secret tls "$NAME_SLUG-internal-ca-key-pair" \
        --cert="$CERT" \
        --key="$KEY" \
        --namespace="${NAMESPACE}"

kubectl -n "$NAMESPACE" delete issuer "$NAME_SLUG-internal-ca-issuer"
echo "--------------------------------------------------------------------------------"

