#!/usr/bin/env bash

NAMESPACE="bk-test"

echo "################################################################################"
echo "Creating CA K8s secrets wildcard blauweknop.app"
echo "################################################################################"


CERT="./pki/blauweknop/demo-wildcard.pem"
KEY="./pki/blauweknop/demo-wildcard-key.pem"

# openssl x509 -in "$CERT" -text -noout

### delete existing secret
kubectl delete secret "wildcard-cert-demo-blauweknop-app" \
        --namespace="${NAMESPACE}"

kubectl create secret tls "wildcard-cert-demo-blauweknop-app" \
        --cert="$CERT" \
        --key="$KEY" \
        --namespace="${NAMESPACE}"

echo "--------------------------------------------------------------------------------"
