#!/usr/bin/env bash

export UPDATE_CERTS=${UPDATE_CERTS:-"false"}
export UPDATE_SCHEMA=${UPDATE_SCHEMA:-"false"}
export NAMESPACE="bk-test"

echo "UPDATE_CERTS: $UPDATE_CERTS"
echo "UPDATE_SCHEMA: $UPDATE_SCHEMA"

if [[ "$(pwd)" =~ .*deployment$ ]]
then
    echo ""
else
  echo "Please execute this script in the deployment folder."
  exit 1
fi

if ! command -v jq &> /dev/null
then
    echo "jq could not be found"
    exit
fi

if ! command -v yq &> /dev/null
then
    echo "yq could not be found"
    exit
fi

if [ -n "${UPDATE_SCHEMA}" ]; then
    echo "[]" > ../scheme/organizations.json
fi

#
# generate.sh NAME OIN IS_REGISTRATOR
#

./script/generate.sh "CJIB" "01634811382079390686"
./script/generate.sh "Gemeente Tilburg" "01634811383832203175" "IS_REGISTRATOR"
./script/generate.sh "UWV" "01634811386140669681"
./script/generate.sh "CAK" "01634811387857829579"
./script/generate.sh "Belastingdienst" "01634811389241598078"
./script/generate.sh "SVB" "01634811390244144305"
./script/generate.sh "DUO" "01634811392636186840"
./script/generate.sh "Waterschap Rivierenland" "01634811394545336563"
./script/generate.sh "Gemeente Maastricht" "01634811395377711843"
./script/generate.sh "Gemeente Coevorden" "01634811397200154955"
./script/generate.sh "Gemeente Utrecht" "01634811399877013576"
./script/generate.sh "Gemeente Rotterdam" "01634811402050713844"
./script/generate.sh "Gemeente Enschede" "01634811403952820608"
./script/generate.sh "Gemeente Baarn" "01634811406189914651"
./script/generate.sh "Gemeente Bergen NH" "01634811408104448031"
./script/generate.sh "Gemeente Zeewolde" "01634811411688100179"
./script/generate.sh "Gemeente Harlingen" "01634811412818552508"
./script/generate.sh "Gemeente Scherpenzeel" "01634811414980171182"
./script/generate.sh "Gemeente Hulst" "01634811415971054765"
./script/generate.sh "LBIO" "01634811417550386402"

unset UPDATE_CERTS
unset UPDATE_SCHEMA
