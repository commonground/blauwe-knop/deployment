#!/usr/bin/env bash
if [[ "$(pwd)" =~ .*"script".* ]]; then
  echo "Do not use this script directly, but use generate_all.sh"
  exit 1
fi

export NAME_SLUG="$1"

if [ -z "$NAME_SLUG" ]
then
	echo "Please set \$NAME_SLUG. `init.sh NAME_SLUG`"
fi

export NAME="$2"

if [ -z "$NAME" ]
then
	echo "Please set \$NAME. `init.sh NAME_SLUG NAME`"
fi


BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "################################################################################"
echo "Generating root CA + intermediate CA for $NAME_SLUG"
echo "################################################################################"
  PKI_DIR="${BASE_DIR}/organizations/${NAME_SLUG}"
  CONFIG="${PKI_DIR}/config.json"
  CA_DIR="${PKI_DIR}/ca"

  cfssl genkey \
    -config "${CONFIG}" \
    -initca \
    "${CA_DIR}/root.json" \
  | cfssljson -bare "${CA_DIR}/root"

  cfssl genkey \
    -config "${CONFIG}" \
    -initca \
    "${CA_DIR}/intermediate.json" \
  | cfssljson -bare "${CA_DIR}/intermediate"

  cfssl sign \
    -config "${CONFIG}" \
    -ca "${CA_DIR}/root.pem" \
    -ca-key "${CA_DIR}/root-key.pem" \
    -profile intermediate \
    "${CA_DIR}/intermediate.csr" \
  | cfssljson -bare "${CA_DIR}/intermediate"

  rm "${CA_DIR}/root.csr"
  rm "${CA_DIR}/intermediate.csr"
echo "--------------------------------------------------------------------------------"
