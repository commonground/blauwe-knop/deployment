#!/usr/bin/env bash
if [[ "$(pwd)" =~ .*"script".* ]]; then
  echo "Do not use this script directly, but use generate_all.sh"
  exit 1
fi

export NAME_SLUG="$1"

if [ -z "$NAME_SLUG" ]
then
	echo "Please set \$NAME_SLUG. `issue.sh NAME_SLUG`"
fi

FORCE=0
if [ "${2}" = "-f" ]; then
  FORCE=1
fi

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "################################################################################"
echo "Issuing certs for $NAME_SLUG"
echo "################################################################################"
PKI_DIR="${BASE_DIR}/organizations/${NAME_SLUG}"
CERT_DIR="${PKI_DIR}/certs"

for CERT_DIR in $(find ${PKI_DIR}/certs/* -type d -maxdepth 0 -print); do
  CERT="$(basename ${CERT_DIR})"
  if [ -f "${CERT_DIR}/cert.pem" ] && [ ${FORCE} == 0 ]; then
    continue
  fi

  echo "Generating certificate for ${CERT}..."

  cfssl gencert \
    -config "${PKI_DIR}/config.json" \
    -ca "${PKI_DIR}/ca/intermediate.pem" \
    -ca-key "${PKI_DIR}/ca/intermediate-key.pem" \
    -profile peer \
    "${CERT_DIR}/csr.json" \
  | cfssljson -bare "${CERT_DIR}/cert"

  cat ${PKI_DIR}/ca/intermediate.pem >> "${CERT_DIR}/cert.pem"

  mv "${CERT_DIR}/cert-key.pem" "${CERT_DIR}/key.pem"
  rm "${CERT_DIR}/cert.csr"

done
echo "--------------------------------------------------------------------------------"
