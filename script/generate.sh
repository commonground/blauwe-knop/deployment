#!/usr/bin/env bash

if [[ "$(pwd)" =~ .*"script".* ]]; then
  echo "Do not use this script directly, but use run.sh"
  exit 1
fi

# exports for the templates
export NAME="$1"
export OIN="$2"
export IS_REGISTRATOR="$3"
export NLX_VERSION="0.118.1"

if [ -z "$NAME" ]
then
	echo "Please set \$NAME. `generate.sh NAME OIN`"
fi
if [ -z "$OIN" ]
then
	echo "Please set \$OIN `generate.sh NAME OIN`"
fi

export NAME_SLUG=$(echo $NAME | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z)
NEW_LINK_REGISTER_API_KEY=$(perl -pe 'binmode(STDIN, ":bytes"); tr/A-Za-z0-9//dc;' < /dev/urandom | fold -w 20 | head -n 1)
NEW_DEBT_REGISTER_API_KEY=$(perl -pe 'binmode(STDIN, ":bytes"); tr/A-Za-z0-9//dc;' < /dev/urandom | fold -w 20 | head -n 1)
NEW_SESSION_REGISTER_API_KEY=$(perl -pe 'binmode(STDIN, ":bytes"); tr/A-Za-z0-9//dc;' < /dev/urandom | fold -w 20 | head -n 1)
export LOGO_URL="https://via.placeholder.com/556x107.png?text=$NAME"

# prepare directories
ROOT=$(dirname $0)
TEMPLATE_DIR="$ROOT/templates"
TEMP_DIRECTORY_PATH=$(mktemp -d)
DEST_DIR="$ROOT/../organizations/$NAME_SLUG"
PWD=$(pwd)
PKI_DIR="$ROOT/../pki/organizations/$NAME_SLUG"
mkdir -p $DEST_DIR

CERTIFICATE_ORGANIZATION_NAME="ProeftuinBlauweKnop$(echo $NAME | sed 's/ //g')"

echo "################################################################################"
echo "$NAME"
echo "################################################################################"

if [[ ! -d "$PKI_DIR" ]]
then
  mkdir -p "$PKI_DIR/ca/"
  cp "$TEMPLATE_DIR/pki/config.json" "$PKI_DIR/config.json"

  SOURCE_FILE="$TEMPLATE_DIR/pki/intermediate"
  DEST_FILE="$PKI_DIR/ca/intermediate.json"
  CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
  awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"

  SOURCE_FILE="$TEMPLATE_DIR/pki/root"
  DEST_FILE="$PKI_DIR/ca/root.json"
  CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
  awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"


  if ! [[ -z "$IS_REGISTRATOR" ]]; then
    mkdir -p "$PKI_DIR/certs/inway"
    SOURCE_FILE="$TEMPLATE_DIR/pki/certs/inway/csr"
    DEST_FILE="$PKI_DIR/certs/inway/csr.json"
    CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
    awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"
  fi

  mkdir -p "$PKI_DIR/certs/management-api"
  SOURCE_FILE="$TEMPLATE_DIR/pki/certs/management-api/csr"
  DEST_FILE="$PKI_DIR/certs/management-api/csr.json"
  CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
  awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"


  mkdir -p "$PKI_DIR/certs/outway"
  SOURCE_FILE="$TEMPLATE_DIR/pki/certs/outway/csr"
  DEST_FILE="$PKI_DIR/certs/outway/csr.json"
  CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
  awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"

  ./pki/init.sh $NAME_SLUG $NAME

  ./pki/issue.sh $NAME_SLUG
fi

if [[ "${UPDATE_CERTS}" != "false" ]]; then
  # generate organization certificate
  openssl req -utf8 -nodes -sha256 -newkey rsa:4096 \
          -keyout "$TEMP_DIRECTORY_PATH/organization-cert.key" \
          -out "$TEMP_DIRECTORY_PATH/organization-cert.csr" \
          -subj "/C=NL/ST=/L=/O=$CERTIFICATE_ORGANIZATION_NAME/OU=/CN=inway-$NAME_SLUG.demo.blauweknop.app"

  ORGANIZATION_CSR_CONTENT_ENCODED=$(jq -aRs . <<< "$(cat $TEMP_DIRECTORY_PATH/organization-cert.csr)")
  ORGANIZATION_CSR_DATA_RAW='{"csr": '$ORGANIZATION_CSR_CONTENT_ENCODED'}'
  curl 'https://certportal.demo.nlx.io/api/request_certificate' -H 'Content-Type: application/json' --data-raw "$ORGANIZATION_CSR_DATA_RAW" | jq -j '.certificate' > $TEMP_DIRECTORY_PATH/organization-cert.crt

  # Make sure we do not store empty certificates
  if [[ -z $(grep '[^[:space:]]' $TEMP_DIRECTORY_PATH/organization-cert.crt) ]] ; then
    echo "Unable to get organization certificate for $CERTIFICATE_ORGANIZATION_NAME"
    exit 1
  fi

  export NLX_ROOT_CERTIFICATE=$(cat "$ROOT/root.crt" | awk '{print "            " $0}')
  export ORGANIZATION_CERTIFICATE=$(cat "$TEMP_DIRECTORY_PATH/organization-cert.crt" | awk '{print "            " $0}')
  export ORGANIZATION_KEY=$(cat "$TEMP_DIRECTORY_PATH/organization-cert.key" | awk '{print "            " $0}')
  export INTERNAL_ROOT_CERTIFICATE=$(cat "$PKI_DIR/ca/root.pem" | awk '{print "            " $0}')
fi


# update scheme
if [ -n "${UPDATE_SCHEMA}" ]; then
  SCHEME_PATH="$ROOT/../../scheme/organizations.json"

  if ! [[ -z "$IS_REGISTRATOR" ]]; then
  cat $SCHEME_PATH | jq \
      --arg oin "$OIN" \
      --arg name "$NAME" \
      --arg apiBaseUrl "https://app-debt-process-$NAME_SLUG.demo.blauweknop.app/api/v1" \
      --arg loginUrl "" \
      --arg appLinkProcessUrl "https://app-link-process-$NAME_SLUG.demo.blauweknop.app/auth/request-link-token" \
      --arg sessionProcessUrl "https://session-$NAME_SLUG.demo.blauweknop.app" \
      --arg isRegistrator true \
      --arg registratorUrl "https://debt-request-process-$NAME_SLUG.demo.blauweknop.app" \
      --arg debtRequestRegisterUrl "" \
      --arg debtRequestRegisterOrganizationName "$CERTIFICATE_ORGANIZATION_NAME" \
      --arg debtRequestRegisterServiceName "$NAME_SLUG-debt-request-register" \
      '. += [{"oin": $oin, "name": $name, "apiBaseUrl": $apiBaseUrl, "loginUrl": $loginUrl, "appLinkProcessUrl": $appLinkProcessUrl, "sessionProcessUrl": $sessionProcessUrl, "isRegistrator": $isRegistrator | test("true"), "registratorUrl": $registratorUrl,  "debtRequestRegisterUrl": $debtRequestRegisterUrl, "debtRequestRegisterOrganizationName": $debtRequestRegisterOrganizationName, "debtRequestRegisterServiceName": $debtRequestRegisterServiceName }]' > "$TEMP_DIRECTORY_PATH/organizations.json"
  else
    cat $SCHEME_PATH | jq \
      --arg oin "$OIN" \
      --arg name "$NAME" \
      --arg apiBaseUrl "https://app-debt-process-$NAME_SLUG.demo.blauweknop.app/api/v1" \
      --arg loginUrl "" \
      --arg appLinkProcessUrl "https://app-link-process-$NAME_SLUG.demo.blauweknop.app/auth/request-link-token" \
      --arg sessionProcessUrl "https://session-$NAME_SLUG.demo.blauweknop.app" \
      '. += [{"oin": $oin, "name": $name, "apiBaseUrl": $apiBaseUrl, "loginUrl": $loginUrl, "appLinkProcessUrl": $appLinkProcessUrl, "sessionProcessUrl": $sessionProcessUrl }]' > "$TEMP_DIRECTORY_PATH/organizations.json"
  fi
    cat "$TEMP_DIRECTORY_PATH/organizations.json" > $SCHEME_PATH
fi

# generate organization files
for filename in $TEMPLATE_DIR/*; do
  [ -f "$filename" ] || continue
  BASE_FILENAME=$(basename $filename)
  SOURCE_FILE="$TEMPLATE_DIR/$BASE_FILENAME"
  DEST_FILE="$DEST_DIR/$BASE_FILENAME.yaml"

  if [ -z "$IS_REGISTRATOR" ] && [ $BASE_FILENAME == "nlx-inway" ]; then
    echo "Creating $BASE_FILENAME.yaml --- SKIPPED: NOT A REGISTRATOR"
    continue
  fi

  if [ -z "$IS_REGISTRATOR" ] && [ $BASE_FILENAME == "debt-request-process" ]; then
    echo "Creating $BASE_FILENAME.yaml --- SKIPPED: NOT A REGISTRATOR"
    continue
  fi
  if [ -z "$IS_REGISTRATOR" ] && [ $BASE_FILENAME == "debt-request-register" ]; then
    echo "Creating $BASE_FILENAME.yaml --- SKIPPED: NOT A REGISTRATOR"
    continue
  fi
  
  echo "Creating $BASE_FILENAME.yaml ..."

  if [[ "${UPDATE_CERTS}" == "false" ]] && [[ $DEST_FILE == *"nlx"* ]] && ! [[ $DEST_FILE == *"-create-"* ]]; then
    echo "------> NOT replacing certificate"
    export NLX_ROOT_CERTIFICATE="$(yq e '.spec.values.tls.organization.rootCertificatePEM' $DEST_FILE   | awk '{ print "          " $0 }' | awk 'NR > 1 { print prev } { prev = $0 }')"
    export ORGANIZATION_CERTIFICATE="$(yq e '.spec.values.tls.organization.certificatePEM' $DEST_FILE   | awk '{ print "          " $0 }' | awk 'NR > 1 { print prev } { prev = $0 }')"
    export ORGANIZATION_KEY="$(yq e '.spec.values.tls.organization.keyPEM' $DEST_FILE                   | awk '{ print "          " $0 }' | awk 'NR > 1 { print prev } { prev = $0 }')"
    export INTERNAL_ROOT_CERTIFICATE="$(yq e '.spec.values.tls.internal.rootCertificatePEM' $DEST_FILE  | awk '{ print "          " $0 }' | awk 'NR > 1 { print prev } { prev = $0 }')"
  elif [[ "${UPDATE_CERTS}" != "false" ]] && [[ $DEST_FILE == *"nlx"* ]] && ! [[ $DEST_FILE == *"-create-"* ]]; then
    echo "------> Replacing certificate"
  fi

  if [[ $DEST_FILE == *"debt-register"* ]]; then
    CURR_DEBT_REGISTER_API_KEY="$(yq e -j $DEST_FILE | jq '.spec.values.config' | jq '.apiKey' | cat)"

    if [ -z "$CURR_DEBT_REGISTER_API_KEY" ]; then
      echo "------> Replacing debt-register apiKey"
      export DEBT_REGISTER_API_KEY="$NEW_DEBT_REGISTER_API_KEY"
    else
      echo "------> NOT replacing debt-register apiKey"
      export DEBT_REGISTER_API_KEY="$CURR_DEBT_REGISTER_API_KEY"
    fi

  fi

  if [[ $DEST_FILE == *"session-register"* ]]; then
    CURR_SESSION_REGISTER_API_KEY="$(yq e -j $DEST_FILE | jq '.spec.values.config' | jq '.apiKey' | cat)"

    if [ -z "$CURR_SESSION_REGISTER_API_KEY" ]; then
      echo "------> Replacing session-register apiKey"
      export SESSION_REGISTER_API_KEY="$NEW_SESSION_REGISTER_API_KEY"
    else
      echo "------> NOT replacing session-register apiKey"
      export SESSION_REGISTER_API_KEY="$CURR_SESSION_REGISTER_API_KEY"
    fi

  fi

  if [[ $DEST_FILE == *"link-register"* ]]; then
    CURR_LINK_REGISTER_API_KEY="$(yq e -j $DEST_FILE | jq '.spec.values.config' | jq '.apiKey' | cat)"

    if [ -z "$CURR_LINK_REGISTER_API_KEY" ]; then
      echo "------> Replacing link-register apiKey"
      export LINK_REGISTER_API_KEY="$NEW_LINK_REGISTER_API_KEY"
    else
      echo "------> NOT replacing link-register apiKey"
      export LINK_REGISTER_API_KEY="$CURR_LINK_REGISTER_API_KEY"
    fi

  fi

  CONTENT=$(envsubst <"$SOURCE_FILE" >"$DEST_FILE")
  awk '{gsub(/\$\{return_code\:\-\}/,"\$return_code")}1' "$DEST_FILE" > temp.txt && mv temp.txt "$DEST_FILE"
done


rm -rf $TEMP_DIRECTORY_PATH

